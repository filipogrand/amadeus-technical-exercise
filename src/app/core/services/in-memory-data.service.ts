import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { SessionService } from './session.service';
import { Passenger } from './../classes/passenger';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor(
    private sessionService: SessionService
  ) { }

  createDb() {
    let passengers = this.sessionService.get('passengers');
    passengers = (passengers === null) ? [] : passengers;
    return { passengers };
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(passengers: Passenger[]): number {
    return passengers.length > 0 ? Math.max(...passengers.map(hero => hero.id)) + 1 : 11;
  }
}
