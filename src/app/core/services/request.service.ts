import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Passenger } from '../classes/passenger';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private http: HttpClient
  ) { }

  private passengersUrl = 'api/passengers';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET: all passengers to the server */
  getPassengers(): Observable<Passenger[]> {
    return this.http.get<Passenger[]>(this.passengersUrl)
      .pipe(
        // tap(_ => console.log('fetched passengers')),
        catchError(this.handleError('getPassengers', []))
      );
  }
  /** POST: add a new passenger to the server */
  addPassenger(passenger: Passenger): Observable<Passenger> {
    return this.http.post<Passenger>(this.passengersUrl, passenger, this.httpOptions)
      .pipe(
        tap(() => console.log(`added passenger w/ id=${passenger.id}`)),
        catchError(this.handleError<Passenger>('addPassenger'))
      );
  }
}
