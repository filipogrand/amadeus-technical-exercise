export class Passenger {
    constructor(
        public id: number,
        public firstName: string,
        public lastName: string,
        public dateOfBirth: string,
        public email?: string,
        public phoneNumber?: string
    ) { }
}
