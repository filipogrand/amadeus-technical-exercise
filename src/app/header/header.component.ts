import { Component, OnInit } from '@angular/core';
import { PassengerService } from 'src/app/passenger/passenger.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private passengerService: PassengerService
  ) { }

  ngOnInit() {
    this.passengerService.getAllState().
      subscribe(state => {
        // console.log(state);
      });
  }

}
