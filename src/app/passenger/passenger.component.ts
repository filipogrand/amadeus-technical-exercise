import { Component, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';
import { SessionService } from './../core/services/session.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Passenger } from '../core/classes/passenger';
import { PassengerService } from './passenger.service';
import { RequestService } from '../core/services/request.service';
import swal from 'sweetalert2';
import * as moment from 'moment';
import {
  ACTION_ADD_PASSENGER,
  ACTION_SAVE_PASSENGER,
  ACTION_DELETE_PASSENGER
} from './store/passengers-actions';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.scss']
})
export class PassengerComponent implements OnInit {
  months = environment.months;
  submitted = false;
  passengersForm: FormGroup;
  passengers: FormArray;
  maxDate = moment(new Date()).format('YYYY-MM-DD');

  constructor(
    private sessionService: SessionService,
    private requestService: RequestService,
    private formBuilder: FormBuilder,
    private passengerService: PassengerService
  ) { }

  ngOnInit() {

    let newId;
    let sessionPassengers;
    this.requestService.getPassengers()
      .subscribe(passengers => {
        sessionPassengers = passengers;
        const formBuilderPassengers = [];
        if (sessionPassengers.passengers) {
          for (const passenger of sessionPassengers.passengers) {
            formBuilderPassengers.push(this.createPassenger(passenger));
          }
          newId = sessionPassengers.passengers.length + 1;

        } else {
          newId = 1;
          const passenger = new Passenger(newId, '', '', '', '', '');
          formBuilderPassengers.push(this.createPassenger(passenger));
        }

        this.passengersForm = this.formBuilder.group({
          passengers: this.formBuilder.array(formBuilderPassengers)
        });
      });
  }

  createPassenger(passenger: Passenger): FormGroup {
    return this.formBuilder.group(passenger);
  }

  addItem(): void {
    this.passengers = this.passengersForm.get('passengers') as FormArray;
    const newId = this.passengers.length + 1;
    const passenger = new Passenger(newId, '', '', '', '', '');
    this.passengers.push(this.createPassenger(passenger));
    this.passengerService.updateState({
      action: ACTION_ADD_PASSENGER,
      payload: this.passengers
    });
  }

  removePassenger(rowIndex: number): void {
    const passengersArray = <FormArray>this.passengersForm.controls['passengers'];
    if (passengersArray.length > 1) {
      swal({
        text: '',
        title: `Are you sure to delete passenger ${rowIndex + 1}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Yes',
        cancelButtonColor: '#DA291C',
        cancelButtonText: 'No',
      }).then((result) => {
        if (result.value) {
          passengersArray.removeAt(rowIndex);
          this.sessionService.set('passengers', this.passengersForm.value, true);
          this.passengerService.updateState({
            action: ACTION_DELETE_PASSENGER,
            payload: this.passengers
          });
          window.scroll(0, 0);
        }
      });

    } else {
      console.log('You cannot delete this row! form should contain at least one row!');
    }
  }

  onSubmit() {
    this.submitted = true;

    // Stop here if form is invalid
    if (this.passengersForm.invalid) { return; }

    this.passengerService.updateState({
      action: ACTION_SAVE_PASSENGER,
      payload: this.passengersForm.value
    });

    this.requestService.addPassenger(this.passengersForm.value as Passenger)
      .subscribe(() => {
        this.sessionService.set('passengers', this.passengersForm.value, true);
        swal({
          text: '',
          title: 'Passengers updated successful!',
          type: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK',
        });
      });


  }
}
