import {
    ACTION_ADD_PASSENGER,
    ACTION_SAVE_PASSENGER,
    ACTION_DELETE_PASSENGER
} from './passengers-actions';

export interface AppReduceState { passengers: {}; }
const initialState = {passengers: {} };

export function Reducer(state = initialState, action): AppReduceState {
    switch (action.type) {

        case ACTION_ADD_PASSENGER:
            console.log('Add Action');
            return {
                ...state,
                passengers: action.payload
            };

        case ACTION_SAVE_PASSENGER:
            console.log('Save Action');
            return {
                ...state,
                passengers: action.payload
            };

        case ACTION_DELETE_PASSENGER:
            console.log('Delete Action');
            return {
                ...state,
                passengers: action.payload
            };

        default: return state;
    }
}
