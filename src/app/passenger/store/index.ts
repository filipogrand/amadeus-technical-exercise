import { Reducer, AppReduceState } from './passangers-reducers';
import { ActionReducerMap } from '@ngrx/store';

interface AppState { appReducer: AppReduceState; }

export const reducers: ActionReducerMap<AppState> = {
    appReducer: Reducer
};
